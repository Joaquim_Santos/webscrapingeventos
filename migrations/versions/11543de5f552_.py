"""empty message

Revision ID: 11543de5f552
Revises: 
Create Date: 2019-08-03 01:40:19.505396

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '11543de5f552'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('atividade',
    sa.Column('ID', sa.Integer(), nullable=False),
    sa.Column('Nome', sa.String(length=200), nullable=False),
    sa.Column('Responsavel', sa.String(length=2000), nullable=True),
    sa.Column('Local', sa.String(length=2000), nullable=True),
    sa.Column('Descricao', sa.String(length=2500), nullable=True),
    sa.Column('Data', sa.Date(), nullable=True),
    sa.Column('Hora_inicio', sa.Time(), nullable=True),
    sa.Column('Hora_fim', sa.Time(), nullable=True),
    sa.PrimaryKeyConstraint('ID')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('atividade')
    # ### end Alembic commands ###
