var quantTextos = 0;
var colecaoTitulos;
var table;
var atividadesOrganizadas;
var classificacaoConteudo;
	
$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	
$(document).ready(function(){
	
	$('#form-link').hide();
	$('#form-dados').hide();
	$('#campos-evento').hide();
	$('#instrucao-dados').hide();
	$('#enviarconteudo').hide();
	$('#trocarTitulo').hide();
	$('#div-tabela').hide();
	$('#confirmacaoConteudo').hide();
	$('#cancelarConteudo').hide();
	
	table = $('#tabela-atividades').DataTable({
		 "language": {
			//"sUrl": "{{url_for('static', filename = 'media/br.js')}}"
			"sEmptyTable":   "Nenhum registro encontrado",
			"sProcessing":   "A processar...",
			"sLengthMenu":   "Mostrar _MENU_ registos",
			"sZeroRecords":  "Não foram encontrados resultados",
			"sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registos",
			"sInfoEmpty":    "Mostrando de 0 até 0 de 0 registos",
			"sInfoFiltered": "(filtrado de _MAX_ registos no total)",
			"sInfoPostFix":  "",
			"sSearch":       "Procurar:",
			"sUrl":          "",
			"oPaginate": {
				"sFirst":    "Primeiro",
				"sPrevious": "Anterior",
				"sNext":     "Seguinte",
				"sLast":     "Último"
			}
			
		},
		"pagingType": "full_numbers"
	});
	
	$('a.toggle-vis').on( 'click', function (e) {
		e.preventDefault();
 
		// Get the column API object
		var column = table.column( $(this).attr('data-column') );
 
		// Toggle the visibility
		column.visible( ! column.visible() );
	} );
	
	document.getElementById('btn-top').click(); 
	
	$('#iniciar').on('click', function(){
		$('#descricao1').hide();
		$('#iniciar').hide();
		$('#dicas').hide();
		$('#form-link').show();
		
	});
	
});

$('#tabela-atividades').on( 'page.dt', function (event) {
    event.returnValue=false;
    event.cancel = true;
    document.getElementById('tituloTabela').click();
} );

$(function() {
	$('#enviarlink').click(function() {
		$("#enviarlink").prop("disabled", true);
		$.ajax({
			url: '/envlink', //No servidor: http://200.131.220.164:8080/hunter/envlink/
			data: $('form').serialize(),
			type: 'POST',
			success: function(response) {
				if(response == 1){
					alert("Link acessado com Sucessso!");
					$("#enviarlink").prop("disabled", false);
					$("#info-link").val("");
					$('#form-link').hide();
					$('#form-dados').show();
				}
				if (response  == 0){
					alert("Nenhum Link enviado!");
				}
				if (response  == 2){
					alert("Link Inválido!");
				}
				if (response  == 3){
					alert("Link não pode ser acessado!!");
				}
				
				$("#enviarlink").prop("disabled", false);
				$("#info-link").val("");
				console.log(response);
			},
			error: function(error) {
				console.log(error);
			}
		});
	});
});

$(function() {
	$('#trocarlink').click(function() {
		$('#form-link').show();
		$('#form-dados').hide();
	});
});


function EnterKeyLink(event, keyCode){
  if (keyCode == 13){  
      event.returnValue=false;
      event.cancel = true;
	  document.getElementById('enviarlink').click(); 
  }
}

function EnterKeyTitle(event, keyCode){  
  if (keyCode == 13){   
      event.returnValue=false;
      event.cancel = true;
	  document.getElementById('enviardados').click(); 
  }
}

$(function() {
	$('#trocarTitulo').click(function() {
		let element = document.getElementById('campos-evento');
		element.innerHTML = '';
		$('#instrucao-dados').hide();
		$('#campos-evento').hide();
		$('#enviarconteudo').hide();
		$('#trocarTitulo').hide()
		$('#form-dados').show();
	});
});

$(function() {
	$('#enviarconteudo').click(function() {
		$("#enviarconteudo").prop("disabled", true);
		$("#trocarTitulo").prop("disabled", true);
		var classificacao = [];
		var achou = 0;
		for (var x = 0; x < quantTextos; x++){
			var texto = "";
			var check = document.getElementsByName("tSignificado" + x.toString()); 
			for (var i=0; i <check.length; i++){ 
				if(check[i].checked == true){
					texto = check[i].value;
					classificacao.push(texto);
				}
			}
			if(texto == ""){
				achou = 1;
				break;
			}
		}
		
		if (achou == 1){
			alert("Um ou mais textos sem marcação!");
		}
		else{
				var conjuntoDados = [];
				conjuntoDados.push(classificacao); 
				conjuntoDados.push(colecaoTitulos);
				classificacaoConteudo = classificacao;
				
				$.ajax({
					url: '/envclassific', //No servidor: http://200.131.220.164:8080/hunter/envclassific/
					contentType: "application/json; charset=utf-8",
					type: 'POST',
					data: JSON.stringify(conjuntoDados),
					success: function(response) {
						
						$("#enviarconteudo").prop("disabled", false);
						$("#trocarTitulo").prop("disabled", false);
						let element = document.getElementById('campos-evento');
						element.innerHTML = '';
						alert("Dados recebidos e analisados com sucesso!");
						$('#instrucao-dados').hide();
						$('#campos-evento').hide();
						$('#enviarconteudo').hide();
						$('#trocarTitulo').hide();
						gera_tabela(response);
						$('#div-tabela').show();
					
					
						console.log(response);
					},
					error: function(error) {
						console.log(error);
					}
			});
			
		}
		$("#enviarconteudo").prop("disabled", false);
		$("#trocarTitulo").prop("disabled", false);
	});
});

function gera_tabela(response) {
	let element = document.getElementById('tabela-texto');
	element.innerHTML = 'Abaixo são exibidas as atividades encontradas, de acordo com os dados informados (Não necessariamente na ordem em que aparecem na página da qual foram retiradas).' + '</br>' + '</br>' +
	'Por favor, verifique se os dados destas estão corretos <a href = "#contact" id = "desviarSecao">(Ver seção de Orientação)</a>. Foram extraídas um total de ' + response['quantidade'] + ' atividades!' + '</br>' + '</br>'; 
	table.clear().draw();
	for (var i = 0; i < response['quantidade']; i++) {
		table.row.add(response['atividades'][i]).draw(false).node();
	}
	atividadesOrganizadas = response['atividades'];
	atividadesOrganizadas.push(classificacaoConteudo);
}

$(function() {
	$('#classificarNovamente').click(function() {
		let element = document.getElementById('tabela-texto');
		element.innerHTML = '';
		$('#form-dados').show();
		$('#div-tabela').hide();
		$('#experimente').click();
	});
});

$(function() {
	$('#confirmarExtracao').click(function() {
		$("#confirmarExtracao").prop("disabled", true);
		$("#cancelarExtracao").prop("disabled", true);
		$("#classificarNovamente").prop("disabled", true);
		
				
		$.ajax({
				url: '/envextract', //No servidor: http://200.131.220.164:8080/hunter/envextract/
				contentType: "application/json; charset=utf-8",
				type: 'POST',
				data: JSON.stringify(atividadesOrganizadas),
				success: function(response) {
					
					$("#confirmarExtracao").prop("disabled", false);
					$("#cancelarExtracao").prop("disabled", false);
					$("#classificarNovamente").prop("disabled", false);
					let element = document.getElementById('tabela-texto');
					element.innerHTML = '';
					$('#confirmacaoConteudo').show();
					$('#div-tabela').hide();
					$('#descobrir').hide();
					document.getElementById('desviar').click(); 
					console.log(response);
				},
				error: function(error) {
					console.log(error);
				}
		});
		
		
		$("#confirmarExtracao").prop("disabled", false);
		$("#cancelarExtracao").prop("disabled", false);
		$("#classificarNovamente").prop("disabled", false);
		$('#experimente').click();
	});
});

$(function() {
	$('#cancelarExtracao').click(function() {
		let element = document.getElementById('tabela-texto');
		element.innerHTML = '';
		$('#cancelarConteudo').show();
		$('#div-tabela').hide();
		$('#descobrir').hide();
		$('#experimente').click();
	});
});

$(function() {
	$('#sonic').click(function() {
		$('#confirmacaoConteudo').hide();
		$('#descobrir').show();
		$('#descricao1').show();
		$('#iniciar').show();
		$('#dicas').show();
		document.getElementById('desviar').click();
	});
});

$(function() {
	$('#silver').click(function() {
		$('#cancelarConteudo').hide();
		$('#descobrir').show();
		$('#descricao1').show();
		$('#iniciar').show();
		$('#dicas').show();
		document.getElementById('desviar').click();
	});
});
