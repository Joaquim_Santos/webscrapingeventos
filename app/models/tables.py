from app import db
from datetime import datetime

class Atividade(db.Model):
    __tablename__= "atividade"
    ID = db.Column(db.Integer, primary_key = True)
    Nome = db.Column(db.String(200), nullable=False)
    Responsavel = db.Column(db.String(2000), nullable=True)
    Local = db.Column(db.String(2000), nullable=True)
    Descricao = db.Column(db.String(2500), nullable=True)
    Data = db.Column(db.Date, nullable=True)
    Hora_inicio = db.Column(db.Time, nullable=True)
    Hora_fim = db.Column(db.Time, nullable=True)

    def __init__(self, Nome, Responsavel, Local, Data, Descricao, Hora_inicio, Hora_fim):
        self.Nome = Nome
        self.Responsavel = Responsavel
        self.Local = Local
        self.Data = Data
        self.Descricao = Descricao
        self.Hora_inicio = Hora_inicio
        self.Hora_fim = Hora_fim

    def __repr__(self):
        return "<Atividade %r>" % self.Nome

