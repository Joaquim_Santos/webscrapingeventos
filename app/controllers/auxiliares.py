# -*- coding: utf-8 -*-
from flask import render_template
from app import app
from flask import request
import urllib3
import json
from http.client import responses
from bs4 import BeautifulSoup
import os
import re


def removeCaraccteres(texto):
    texto2 = ''
    for caractere in texto:
        if (caractere == '\'' or caractere == '\"' or caractere == '(' or caractere == ')' or caractere == '[' or caractere == ']' or caractere == '*' or caractere == '$' or caractere == '\\'):
            texto2 = texto2 + '\\' + caractere
        else:
            texto2 = texto2 + caractere

    return texto2



def removeExpacoDuplo(texto):
    contador = -1
    texto2 = ''
    for caractere in texto:
        contador = contador + 1
        if (caractere == ' ' and contador < (len(texto) - 1)):
            if (texto[contador + 1] == ' '):
                texto2 = texto[0:contador]
                return texto2

    return texto




def extrairAtributos(texto, soup):
    texto2 = removeCaraccteres(texto)
    texto2 = removeExpacoDuplo(texto2)

    extracaoTexto = soup(text=re.compile(texto2, re.IGNORECASE))

    chaves = []
    for elem in extracaoTexto:
        #print("Tag do elemento")
        tag = str(elem.parent).split(' ')
        tag = tag[0] + '>'
        #print(tag)

        #print("atributos do texto")
        atributos = elem.parent.attrs
        #print(atributos)

        chaves.append(tag)
        for atr in atributos:
            chaves.append(str(atr).replace('\'', ''))

        #print("Chaves do texto")
        #print(chaves)

        break

    return chaves




def buscarTitulos(soup, chaves, chavesTexto):
       chaves2 = []
       texto = soup.find_all(chaves[0].replace('<', '').replace('>', ''))
       cont = -1
       textoVerificador = str(soup.get_text()).split('\n')
       valor = ''
       while valor in textoVerificador:
           textoVerificador.remove(valor)

       colecao = []
       for frase in texto:
           frase = str(frase.text.strip())
           if (frase != ''):
               frase2 = removeCaraccteres(frase)
               frase3 = removeExpacoDuplo(frase2)
               classificacao = soup(text=re.compile(frase3, re.IGNORECASE))

               for elem in classificacao:
                   tag = str(elem.parent).split(' ')
                   tag = tag[0] + '>'
                   atributos = elem.parent.attrs
                   chaves2.append(tag)
                   for atr in atributos:
                       chaves2.append(str(atr).replace('\'', ''))

                   break

               if(len(chaves) == len(chaves2)):
                   lista_final = list(set(chaves2) - set(chaves))
                   if(len(lista_final) == 0):
                       for fraseVerificadora in textoVerificador:
                           cont = cont + 1
                           if (fraseVerificadora.upper().replace(' ', '') == frase.upper().replace(' ', '')):
                               #print("Conteúdo posterior ao título sendo verificado")
                               #print(fraseVerificadora)
                               #print(textoVerificador[cont + 1])
                               chavesTextoVerificado = extrairAtributos(textoVerificador[cont + 1], soup)
                               #print(chavesTextoVerificado)
                               if (chavesTextoVerificado == chavesTexto):
                                   colecao.append(frase)
                               break
                       cont = -1


               chaves2.clear()
       return colecao


def extrairData(data):
    print('Extraindo Data')
    data_obj = re.search(r'([0-9][0-9]\ \w+)|([0-9]\ \w+)', data)
    print('Tentando primeiro formato')
    if(data_obj != None):
        print(data_obj.group(0))
        data = str(data_obj.group(0)).split(' ')
        colecaoMeses = ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro',
                        'novembro', 'dezembro']
        i = 1
        for mes in colecaoMeses:
            if mes == data[1 ].lower():
                break
            i = i + 1
            if (i == 13):
                return ''
        digito = ''
        if (i < 10):
            digito = '0'
        digito2 = ''
        if (int(data[0]) < 10):
            digito2 = '0'
        data = digito + str(i) + '-' + digito2 + data[0]
        print(data)
        return data

    else:
        data_obj = re.search(r'(\w+\ [0-9][0-9])|(\w+\ [0-9])', data)
        print('Tentando segundo formato')
        if (data_obj != None):
            print(data_obj.group(0))
            data = str(data_obj.group(0)).split(' ')
            colecaoMeses = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october',
                            'november', 'dezember']
            i = 1
            for mes in colecaoMeses:
                if mes == data[0].lower():
                    break
                i = i + 1
                if (i == 13):
                    return ''
            digito = ''
            if (i < 10):
                digito = '0'
            digito2 = ''
            if (int(data[1]) < 10):
                digito2 = '0'
            data = digito + str(i) + '-' + digito2 + data[1]
            print(data)
            return data
        else:
            data_obj = re.search(r'([0-9][0-9]\/[0-9][0-9]\/[0-9][0-9][0-9][0-9])|([0-9][0-9]\/[0-9][0-9])', data)
            print('Tentando terceiro formato')
            if (data_obj != None):
                print(data_obj.group(0))
                data = str(data_obj.group(0)).split('/')
                data = data[::-1]
                dataFormatada = ''
                for x in range(0, len(data)):
                    dataFormatada = dataFormatada + data[x]
                    if (x != len(data) - 1):
                        dataFormatada = dataFormatada + '-'
                print(dataFormatada)
                return dataFormatada
            else:
                data_obj = re.search(r'([0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9])|([0-9][0-9]\-[0-9][0-9])', data)
                print('Tentando quarto formato')
                if (data_obj != None):
                    print(data_obj.group(0))
                    data = str(data_obj.group(0))
                    print(data)
                    return data
                else:
                    print('Nenhum formato padrão identificado!')
                    return ''

def extrairHora(hora, data):
    print('Extraindo hora')
    print('Tentando pela data')
    hora_obj = re.search(r'([0-9][0-9]\:[0-9][0-9])|([0-9][0-9]h)|([0-9][0-9]\ h)', data.lower())
    if(hora_obj != None):
        print(hora_obj.group(0))
        hora = str(hora_obj.group(0))
        if(len(hora) < 5):
            hora = hora[0] + hora[1] + ':00'
            print(hora)
        return hora
    else:
        print('Tentando pela hora')
        hora_obj = re.search(r'([0-9][0-9]\:[0-9][0-9])|([0-9][0-9]h)|([0-9][0-9]\ h)', hora.lower())
        if (hora_obj != None):
            print(hora_obj.group(0))
            hora = str(hora_obj.group(0))
            if (len(hora) < 5):
                hora = hora[0] + hora[1] + ':00'
                print(hora)
            return hora
        else:
            print('Nenhum formato padrão identificado!')
            return ''

def compararString(a, b):
    i = 0

    if(len(a) != len(b)):
        return False
    while i < len(a):
        print (i)
        if a[i] != b[i]:
            return False
            break
        i += 1
    return True
