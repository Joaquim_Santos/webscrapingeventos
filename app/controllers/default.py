# -*- coding: utf-8 -*-
from .auxiliares import *
from flask import jsonify
from app import app, db
from collections import OrderedDict
from app.models.tables import Atividade
import datetime
import re

@app.route('/home',methods = ['POST', 'GET'])
@app.route("/", methods = ['POST', 'GET'])
def index():
    return render_template('home.html')

@app.route('/envlink/', methods = ['POST', 'GET'])
def envlink():
        if request.method == 'GET':
            return render_template('home.html')

        print("Link Recebido")
        info_link = str(request.form['info-link'])
        print(info_link)
        codigo = 1
        if (info_link == ''):
            codigo = 0
            return json.dumps(codigo)

        http = urllib3.PoolManager()
        try:
            requesicao = http.request('GET', info_link)
            http_status = requesicao.status
            http_status_description = responses[http_status]
            print(http_status)
            print(http_status_description)
            if(int(http_status) >= 300):
                print("Erro! URL alterada ou não pode ser acessada!")
                codigo = 3
        except:
            print("Falha ao acessar link!")
            codigo = 2

        if(codigo == 1):
              data = requesicao.data.decode("utf-8", errors='ignore')
              filename = "app/templates/PageExtract.txt"
              file = open(filename, 'wt', encoding="utf-8")
              for palavra in data:
                file.write(palavra)
                if(str(palavra) == '>'):
                    file.write('\n')

              file.close()

        return json.dumps(codigo)


@app.route('/envdados/', methods=['POST', 'GET'])
def envdados():
    if request.method == 'GET':
            return render_template('home.html')

    print("Formulário de dados Recebido")
    titulo = str(request.form['info-titulo'])
    print(titulo)

    codigo = 1
    conteudoTitulo = []

    if (titulo == '' or len(titulo) < 10):
        codigo = 0
        return jsonify({'confirma': codigo, 'conteudo': conteudoTitulo})


    os.chdir('app/templates')
    print("Aberto diretório da página!")

    arqHtml = open('PageExtract.txt', 'rt', encoding="utf-8")
    textoHtml = arqHtml.read()
    arqHtml.close()

    os.chdir('../..')

    soup = BeautifulSoup(textoHtml, "html.parser")

    titulo2 = removeCaraccteres(titulo)
    titulo2 = removeExpacoDuplo(titulo2)


    extracaoTitulo = soup(text=re.compile(titulo2, re.IGNORECASE))


    #print(soup(text=re.compile('Lívia Christinie Teles and Alexandre Fernandes \(Federal University of Ceará, Brazil\);')))
    if(len(extracaoTitulo) == 0):
        codigo = 2
        return jsonify({'confirma': codigo, 'conteudo': conteudoTitulo})

    chaves = []
    for elem in extracaoTitulo:
         print("Tag do elemento")
         tag = str(elem.parent).split(' ')
         tag = tag[0] + '>'
         print(tag)

         print("atributos do título")
         atributos = elem.parent.attrs
         print(atributos)

         chaves.append(tag)
         for atr in atributos:
             chaves.append(str(atr).replace('\'', ''))

         print("Chaves do título")
         print(chaves)

         break

    cont = -1
    texto = str(soup.get_text()).split('\n')
    valor = ''
    chavesTexto = []
    while valor in texto:
        texto.remove(valor)
    for frase in texto:
        cont = cont + 1
        frase = removeCaraccteres(frase)
        frase = removeExpacoDuplo(frase)

        if (compararString(frase.lower().replace(' ', ''), titulo2.lower().replace(' ', ''))):
            print("Conteúdo posterior ao título")
            print(frase)
            print(texto[cont + 1])
            chavesTexto = extrairAtributos(texto[cont + 1], soup)
            print(chavesTexto)
            break


    colecaoTitulos = buscarTitulos(soup, chaves, chavesTexto)
    print("Títulos dos Eventos")
    print(colecaoTitulos)
    if (len(colecaoTitulos) == 0):
        codigo = 2
        return jsonify({'confirma': codigo, 'conteudo': conteudoTitulo})
    cont = -1
    achou = False
    isTitulo = False
    for frase in texto:
        cont = cont + 1
        if (frase.upper() == colecaoTitulos[0].upper()):
            print("Primeiro título encontrado")
            while True:
                cont = cont + 1
                for titulo in colecaoTitulos:
                    if (texto[cont].replace(' ', '') == titulo.replace(' ', '')):
                        isTitulo = True
                        break

                if (not isTitulo):
                    conteudoTitulo.append(texto[cont])
                else:
                    achou = True
                    break
        if (achou):
            break

    print("Conteúdo do primeiro título")
    print(conteudoTitulo)

    return jsonify({'confirma': codigo, 'conteudo': conteudoTitulo, 'colecao': colecaoTitulos})


@app.route('/envclassific/', methods=['POST', 'GET'])
def envclassific():
    if request.method == 'GET':
            return render_template('home.html')

    print("Conteúdo do título recebido!")
    conjuntoDados = request.data
    conjuntoDados = str(conjuntoDados.decode('utf-8'))
    conjuntoDados = conjuntoDados.split('],')

    conteudoTitulos = conjuntoDados[0].replace('[', '').replace(']', '').replace('\"', '').split(',')
    colecaoTitulos = conjuntoDados[1].replace('[', '').replace(']', '').split('\"')
    valor = ','
    while valor in colecaoTitulos:
        colecaoTitulos.remove(valor)

    colecaoTitulos.remove('')
    colecaoTitulos.remove('')

    os.chdir('app/templates')
    print("Aberto diretório da página!")

    arqHtml = open('PageExtract.txt', 'rt', encoding="utf-8")
    textoHtml = arqHtml.read()
    arqHtml.close()

    os.chdir('../..')

    soup = BeautifulSoup(textoHtml, "html.parser")

    texto = str(soup.get_text()).split('\n')
    valor = ''
    while valor in texto:
        texto.remove(valor)

    cont = -1
    quantTitulos = 0
    conteudoAtividade = []
    terminou = False
    isTitulo = False
    listaAtividades = []
    for frase in texto:
        cont = cont + 1
        if (frase.upper() == colecaoTitulos[0].upper()):
            print("título encontrado Para Início da extração!")
            print('\n\n\n')
            conteudoAtividade.append(frase)
            while True:
                if (quantTitulos == (len(colecaoTitulos) - 1)):
                    terminou = True
                    for i in range(0, len(conteudoTitulos)):
                        cont = cont + 1
                        try:
                            conteudoAtividade.append(texto[cont])
                        except:
                            terminou = True
                            break

                if (terminou):
                    print(conteudoAtividade)
                    print('\n\n')
                    listaAtividades.append(conteudoAtividade[:])
                    conteudoAtividade.clear()
                    break

                cont = cont + 1
                for titulo in colecaoTitulos:
                    try:
                        if (texto[cont].replace(' ', '') == titulo.replace(' ', '')):
                            isTitulo = True
                            break
                    except:
                        terminou = True
                        break

                if(not isTitulo):
                        try:
                            conteudoAtividade.append(texto[cont])
                        except:
                            terminou = True
                            break
                else:
                    isTitulo = False
                    quantTitulos = quantTitulos + 1
                    if(quantTitulos == 1):
                        tamanhoATividade = len(conteudoAtividade)

                    if(len(conteudoAtividade) == tamanhoATividade):
                        listaAtividades.append(conteudoAtividade[:])
                    '''
                    else:
                        print("Atividade Fora do Padrão")
                        print(conteudoAtividade)
                    '''
                    conteudoAtividade.clear()
                    try:
                        conteudoAtividade.append(texto[cont])
                    except:
                        terminou = True
                        break

        if(terminou):
            break

    '''
    print('\n\nAtividades coletadas!')
    for atividade in listaAtividades:
        print(atividade)
        print('\n\n')
    '''
    print('Coletadas %i Atividades!' %len(listaAtividades))
    print('Organizando Atividades\n\n')

    atividadesOrganizadas = []
    titulo = data = horaInicio = horaFim  = autor = local = descricao = ''
    for atividade in listaAtividades:
        titulo = titulo + atividade[0]
        for x in range(0, len(conteudoTitulos)):
            if (conteudoTitulos[x].lower() == 'autor'):
                autor = autor + atividade[x + 1] + ' '

            elif (conteudoTitulos[x].lower() == 'data'):
                data = data + atividade[x + 1] + ' '

            elif (conteudoTitulos[x].lower() == 'hora início'):
                horaInicio = horaInicio + atividade[x + 1] + ' '

            elif (conteudoTitulos[x].lower() == 'hora fim'):
                horaFim = horaFim + atividade[x + 1] + ' '

            elif (conteudoTitulos[x].lower() == 'local'):
                local = local + atividade[x + 1] + ' '

            elif (conteudoTitulos[x].lower() == 'descrição'):
                descricao = descricao + atividade[x + 1] + ' '

        atividadesOrganizadas.append([titulo, data, horaInicio, horaFim, autor, descricao, local])
        print([titulo, data, horaInicio, horaFim, autor, descricao, local])
        print('\n\n')
        titulo = data = horaInicio = horaFim = autor = local = descricao = ''

    print('Organizadas %i Atividades!' % len(atividadesOrganizadas))
    return jsonify({'atividades': atividadesOrganizadas, 'quantidade': len(atividadesOrganizadas)})

@app.route('/envextract/', methods=['POST', 'GET'])
def envextract():
    if request.method == 'GET':
            return render_template('home.html')
            
    print("\n\nPreparando para armazenar!\n\n")
    atividadesOrganizadas = request.data
    atividadesOrganizadas = str(atividadesOrganizadas.decode('utf-8'))
    atividadesOrganizadas = atividadesOrganizadas.split('],')

    atividadesExtraidas = []

    for atividade in atividadesOrganizadas:
        atividade2 = atividade.replace('[', '').replace(']', '').split('\"')
        valor = ','
        while valor in atividade2:
            atividade2.remove(valor)

        atividade2.remove('')
        atividade2.remove('')
        atividadesExtraidas.append(atividade2)

    for novaAtividade in atividadesExtraidas:
        print(novaAtividade)
        print("\n\n")

    classificacaoConteudo = atividadesExtraidas[len(atividadesExtraidas) - 1]
    del atividadesExtraidas[len(atividadesExtraidas) - 1]
    print('Extraídas %i Atividades!' % len(atividadesExtraidas))
    print(classificacaoConteudo)
    classificacaoConteudo = list(OrderedDict.fromkeys(classificacaoConteudo).keys())
    print(classificacaoConteudo)
    try:
        classificacaoConteudo.remove('Descartar')
    except:
        pass

    print(classificacaoConteudo)

    print("\n\nIniciar agrupamento para armazenar em Banco\n\n")
    titulo = data = horaInicio = horaFim = autor = local = descricao = ''
    x = 0
    for novaAtividade in atividadesExtraidas:
        titulo = titulo + novaAtividade[0]
        for i in range(1, len(novaAtividade)):
            if (novaAtividade[i] != ''):
                if (classificacaoConteudo[x].lower() == 'autor'):
                    autor = autor + novaAtividade[i]
                    x = x + 1
                elif (classificacaoConteudo[x].lower() == 'data'):
                    data = data + novaAtividade[i]
                    x = x + 1
                elif (classificacaoConteudo[x].lower() == 'hora início'):
                    horaInicio = horaInicio + novaAtividade[i]
                    x = x + 1
                elif (classificacaoConteudo[x].lower() == 'hora fim'):
                    horaFim = horaFim + novaAtividade[i]
                    x = x + 1
                elif (classificacaoConteudo[x].lower() == 'local'):
                    local = local + novaAtividade[i]
                    x = x + 1
                elif (classificacaoConteudo[x].lower() == 'descrição'):
                    descricao = descricao + novaAtividade[i]
                    x = x + 1

        print([titulo, data, horaInicio, horaFim, autor, descricao, local])
        print('\n\n')
        dataAux = data
        data = extrairData(data)
        if (data != '' and len(data) < 10):
            data = str(datetime.date.today().year) + '-' + data
        horaInicio = extrairHora(horaInicio, dataAux)
        horaFim = extrairHora(horaFim, dataAux)
        if (horaInicio == ''):
            horaInicio = None
        if (horaFim == ''):
            horaFim = None
        if (data == ''):
            data = None
        try:
            atividadeParaBD = Atividade(titulo, autor, local, data, descricao, horaInicio, horaFim)
            db.session.add(atividadeParaBD)
            db.session.commit()
        except:
            pass
        titulo = data = horaInicio = horaFim = autor = local = descricao = ''
        x = 0

    return jsonify(1)